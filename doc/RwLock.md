# RwLock: Reader-Writer lock

## Introduction
We can simply protect a shared resource by a lock. But the performance is not
good because each reader should run one-by-one.

A Reader-Writer lock can improve the performance by let readers running 
simultaneously.

By the way, a writer should wait until all readers done.
In a frequently read situation, a new reader after the writer can also increase
the read count, let read count never decrease to 0. 
This will starve writer.

RwLock:
1. Let readers running simultaneously.
1. Exclude "multiple readers" and each writer.
1. provide a flag "write_first" to prevent starve writer.

After 1.6.0, RwLock can support multi-thread(default) and multi-process.
For multi-process scenario, use `rwlock = RwLock(cross_process=True)`


## Usage

### Install
1. `pip install cylibs`
1. download latest version from https://pypi.org/project/cylibs

#### Example
```
from cylibs import RwLock

rwlock = RwLock()

### READER
rwlock.acquire_r()
# read...
rwlock.release_r()

# OR
with rwlock.lock_r():
    # read...

### WRITER
rwlock.acquire_w()
# write...
rwlock.release_w()

# OR
with rwlock.lock_w():
    # write...
```
