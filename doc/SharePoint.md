# SharePoint

## Introduction


## Usage
### Install
1. `pip install cylibs`
1. download latest version from https://pypi.org/project/cylibs

#### Example
```
from cylibs import SharePoint

url='<URL>'
site='sites/<sitename>'
user='<domain>\<username>'
pwd='<password>'
sp = SharePoint(url, site, user, pwd, debug=True)


# list
remote_folder='<remote folder path>'
sp.list(remote_folder)

# download
remote_file='<remote file for download>'
local_file='<the target file>'
sp.download(remote_file, local_file)

# upload
remote_folder='<remote folder for upload>'
local_file='<local file to upload>'
sp.upload(remote_folder, local_file)

# delete
remote_file='<remote file for delete>'
sp.delete(remote_file)
```