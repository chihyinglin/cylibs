# cylibs

My python libraries

[![pipeline status](https://gitlab.com/chihyinglin/cylibs/badges/main/pipeline.svg)](https://gitlab.com/chihyinglin/cylibs/-/commits/main)
[![coverage report](https://gitlab.com/chihyinglin/cylibs/badges/main/coverage.svg)](https://gitlab.com/chihyinglin/cylibs/-/commits/main)

## Modules
- [RwLock](https://gitlab.com/chihyinglin/cylibs/-/blob/main/doc/RwLock.md)
  - A Python Reader-Writer lock.
- [SharePoint](https://gitlab.com/chihyinglin/cylibs/-/blob/main/doc/SharePoint.md)
  - A SharePoint library to do List/Download/Upload/Delete.

### Install
1. `pip install cylibs`
2. download latest version from https://pypi.org/project/cylibs
